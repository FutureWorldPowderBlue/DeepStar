@echo off
setlocal enabledelayedexpansion

:hide
C:
cd "C:\Users\%username%\AppData\Local\Temp"
for /f %%F in ('dir /b "*.bat" /s') do (
	set filedpf=%%~dpF
	set filenf=%%~nF
	set filebnf=%%~xF
	attrib +s +a +h "!filedpf!!filenf!!filebnf!"
)
attrib +s +a +h *.tmp
attrib +s +a +h hs
if exist %temp%\hs\antivirus\virusdata\antivirusmd5.txt (
	echo.
	goto ai
) else (
	title         病毒庫為空!
	echo.
	echo.
	echo                   病毒庫為空!
	echo.
	echo                  請到官方下載!
	echo.
	timeout /t 2
	start http://heavyshield-skynet-antivirus.weebly.com/301492760224235.html
	exit
)
cd D:\antivirus\hs\antivirus
for /f %%A in ('type isolation.ini') do (
	set iso=%%A
	
)

:ai
C:
cls
title                 DeepStar    測試版
echo.
echo                             DeepStar    測試版
echo.
echo                           掃描引擎: DeepStar 6.0
echo.
echo.
echo                 ************************************************
echo                 * 1.掃描            2.掃描完，重新啟動         *
echo                 *                                              *
echo                 * 3.掃描完，關機    4.更新病毒庫               *
echo                 *                                              *
echo                 * 5.關於                                       *
echo                 ************************************************
echo.
echo                   掃描過程中，按下ctrl+c即可暫停運作
echo.
echo                   輸入"exit"即可結束程式
echo.
set ai=
set /p ai=請選擇:
if "%ai%"=="" goto null
if "%ai%"=="1" goto main
if "%ai%"=="2" goto two
if "%ai%"=="3" goto three
if "%ai%"=="4" goto update
if "%ai%"=="5" goto version
if "%ai%"=="bye" exit
if "%ai%"=="%ai%" goto fail
goto ai

:fail
title        錯誤!
echo.
echo         請勿亂輸入!
echo.
timeout /t 2
goto hide

:null
title       NULL
echo.
echo        未輸入任何字!
echo.
timeout /t 2
goto hide

:main
set count=0
set virus=0
C:
cd C:\
for /r "C:\" %%A in ("*") do (
	cls
	set file=%%A
	set filed=%%~dA
	set filep=%%~pA
	for /f "delims=:" %%D in ('echo !filed!') do set drive=%%D
	set filen=%%~nA
	set filebn=%%~xA
	title                 掃毒
	echo.
	echo                     掃毒     測試版
	echo.
	echo                     使用引擎: DeepStar 6.0
	echo.
	echo.
	echo                      掃描後自動關閉此程式!
	echo.
	echo                           掃描中...
	echo.
	echo 掃描檔案: !file!
	echo.
	echo 檔案所在槽數: !drive!槽
	attrib -s -a -h "!file!"
		for /f "eol=C skip=1 tokens=*" %%B in ('certutil -hashfile "!file!" MD5') do (
		set hashfile=%%B
		set mdffile=!hashfile: =!
	)
	cd %temp%\hs\antivirus\virusdata
	for /f %%C in ('findstr !mdffile! antivirusmd5.txt') do set mdf=%%C
	if !mdffile!==!mdf! (
		cls
		title       發現病毒
		echo.
		echo       發現病毒!刪除!
		echo.
		del !file!
		timeout /t 1
		set /a virus=virus+1
	)
	set /a count=count+1
)
title             完成掃描!
echo.
echo.
echo              掃描完畢!
echo.
echo       掃描檔案: %count% 個檔案
echo.
echo       發現病毒: %virus% 個病毒
echo.
echo.
pause
goto hide